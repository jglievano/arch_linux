# Arch Linux setup instructions

This instructions are for installing Arch Linux in a Lenovo X1 Carbon Gen 5
specifically.

1. First thing is to have the internet connection problem solved.

        bash
        systemctl stop dhcpcd@ # [tab]
        wifi-menu

1. Activate network time synchronization.

        timedatectl set-ntp true

1. Setup disk partitions.

    1. **EFI System**: `400M`
    1. **Linux swap**: `2G`
    1. **Linux filesystem**: remaining space

            fdisk -l
            cgdisk # to setup.
            mkfs.fat -F32 /dev/efi_partition
            mkswap /dev/swap_partition
            swapon /dev/swap_partition
            mkfs.ext4 /dev/root_partition

1. Mount, install `base` and change the apparent root directory.

        mount /dev/root_partition /mnt
        mkdir /mnt/boot
        mount /dev/efi_partition /mnt/boot
        pacstrap /mnt base base-devel
        genfstab -U /mnt >> /mnt/etc/fstab
        arch-chroot /mnt

1. Update Arch Linux mirrorlist.

        mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.orig
        curl -s "htps://www.archlinux.org/mirrorlist/?country=US&protocol=https&use_mirror_status=on" \
          | sed -e 's/^#Server/Server/' -e '/^#/d' \
          | rankmirrors -n 20 - >> /etc/pacman.d/mirrorlist

    Uncomment `multilib` and add `yaourt` repository to `/etc/pacman.conf`.

        [multilib]
        Include = /etc/pacman.d/mirrorlist

        [archlinuxfr]
        SigLevel = Never
        Server = http://repo.archlinux.fr/$arch

    Finally, update pacman and install `yaourt`.

        pacman -Sy yaourt

1. Set up bigger fonts.

        pacman -S vim terminus-font ttf-dejavu noto-fonts ttf-roboto
        yaourt -S ttf-font-awesome-4 # v5 has issues with spaces.
        setfont ter-132n

    Edit `/etc/vconsole.conf` too.

        FONT=ter-132n

1. Finish system configuration.

        ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
        hwclock --systohc

    Uncomment `en_US.UTF-8 UTF-8` from `/etc/locale.gen` and edit
    `/etc/locale.conf`.

        LANG=en_US.UTF-8

    Then run

        locale-gen

    Edit `/etc/hostname`:

        myhostname

    Edit `/etc/hosts`:

        127.0.0.1 localhost
        ::1 localhost
        127.0.1.1 myhostname.localdomain myhostname

    Create user.

        useradd -m -g users -G wheel,storage,power -s /bin/bash jgl
        passwd jgl
        pacman -S sudo

    Uncomment `%wheel ALL=(ALL) ALL`:

        visudo

    And finalize installing the boot loader.

        pacman -S grub efibootmgr
        grub-install \
          --target=x86_64-efi \
          --efi-directory=/boot \
          --bootloader-id=arch_grub
        grub-mkconfig -o /boot/grub/grub.cfg
        pacman -S intel-ucode

1. Install network utilities.

        pacman -S iw wpa_supplicant dialog
        pacman -S networkmanager network-manager-applet

1. Install `xorg` and UI stuff.

        pacman -S alsa-utils alsa-firmware alsa-plugins \
          pulseaudio-alsa pulseaudio
        pacman -S xorg xorg-server xorg-xinit xterm
        pacman -S xf86-video-intel
        pacman -S dmenu rofi exa termite ranger
        pacman -S lxappearance
        pacman -S adapta-gtk-theme papirus-icon-theme
        pacman -S playerctl
        pacman -S i3

1. Install keyring.

        pacman -S libsecret gnome-keyring seahorse

1. Install editor.

        pacman -S emacs

1. Reboot

        exit
        umount -R /mnt
        reboot

# Install Yaourt packages

    yaourt -S pa-applet
